using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LabelEpitel : MonoBehaviour
{
    private Transform cam;
    [SerializeField] private Epitel nama;

    [SerializeField] private RokokPage page;

    private void Start()
    {
        cam = Camera.main.transform;
    }

    private void Update()
    {
        transform.LookAt(2 * transform.position - cam.position);
    }

    private void OnMouseEnter()
    {
        page.SetDetailEpitel(nama);
        GetComponent<TMP_Text>().color = Color.red;
    }

    private void OnMouseExit()
    {
        GetComponent<TMP_Text>().color = Color.white;
    }
}

public enum Epitel
{
    Penurunan,
    Abnormalitas,
    Peningkatan
}

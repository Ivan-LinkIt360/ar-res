using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelPanelA : MonoBehaviour
{
    [SerializeField] private Button b_back;
    [SerializeField] private Button b_help;
    [SerializeField] private Button b_answer;

    [SerializeField] private TMP_Dropdown[] dropdownsPendek;
    [SerializeField] private TMP_Dropdown[] dropdownsPanjang;

    [SerializeField] private GameObject panel_result;
    [SerializeField] private TMP_Text label_result;

    [SerializeField] private GameObject panelInfo;
    [SerializeField] private Button b_closeInfo;

    private void Start()
    {
        b_back.onClick.AddListener(() => gameObject.SetActive(false));
        b_help.onClick.AddListener(() => panelInfo.SetActive(true));
        b_closeInfo.onClick.AddListener(() => panelInfo.SetActive(false));
        b_answer.onClick.AddListener(CekJawaban);
    }

    private void CekJawaban()
    {
        List<int> pendek = new List<int>();
        List<int> panjang = new List<int>();

        for (int i = 0; i < dropdownsPendek.Length; i++)
        {
            pendek.Add(dropdownsPendek[i].value);
        }
        for (int i = 0; i < dropdownsPanjang.Length; i++)
        {
            panjang.Add(dropdownsPanjang[i].value);
        }

        bool jawabanPendek = pendek[0] == 0 && pendek[1] == 1 && pendek[2] == 2 &&
            pendek[3] == 3 && pendek[4] == 4 && pendek[5] == 5 &&
            pendek[6] == 6 && pendek[7] == 7 && pendek[8] == 8;
        bool jawabanPanjang = panjang[0] == 0 && panjang[1] == 1 && panjang[2] == 2 &&
            panjang[3] == 3 && panjang[4] == 4 && panjang[5] == 5;

        if (jawabanPendek && jawabanPanjang)
        {
            MunculkanHasil(true);
        }
        else
        {
            MunculkanHasil(false);
        }
    }

    public void MunculkanHasil(bool benar)
    {
        panel_result.SetActive(true);
        if(benar)
        {
            AudioManager.Instance.MainkanSuara("win");
            label_result.text = $"<color=green>Jawaban Benar !!";
        }
        else
        {
            AudioManager.Instance.MainkanSuara("lose");
            label_result.text = $"<color=red>Jawaban Salah . .";
        }

        StartCoroutine(HideResult());
    }

    public void CheckJawabanPendek(int jawaban)
    {
        
    }

    IEnumerator HideResult()
    {
        yield return new WaitForSeconds(3f);
        panel_result.SetActive(false);
    }
}

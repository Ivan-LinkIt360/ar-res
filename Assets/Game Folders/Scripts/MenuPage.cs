using Dev.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuPage : Page
{
    [SerializeField] private Button b_exit;
    [SerializeField] private Button b_pendahuluan;
    [SerializeField] private Button b_petunjuk;
    [SerializeField] private Button b_kameraAr;
    [SerializeField] private Button b_kuis;
    [SerializeField] private Button b_info;
    [SerializeField] private Button b_pengaturan;

    private void Start()
    {
        b_exit.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Quit));

        b_pendahuluan.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Pendahuluan));
        b_petunjuk.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Petunjuk));
        b_kameraAr.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.KameraAr));
        b_kuis.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Kuis));
        b_info.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Info));
        b_pengaturan.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Pengaturan));

        AudioManager.Instance.MainkanSuara("BGM");
    }
}

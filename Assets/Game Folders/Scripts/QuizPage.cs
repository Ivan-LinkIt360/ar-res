using Dev.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuizPage : Page
{
    [SerializeField] private Button b_home;

    [SerializeField] private Button b_levelA;
    [SerializeField] private Button b_levelB;

    [SerializeField] private GameObject LevelA;
    [SerializeField] private GameObject LevelB;

    private void Start()
    {
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        b_levelA.onClick.AddListener(() =>
        {
            LevelA.SetActive(true);
            LevelB.SetActive(false);
        });

        b_levelB.onClick.AddListener(() =>
        {
            LevelA.SetActive(false);
            LevelB.SetActive(true);
        });
    }
}

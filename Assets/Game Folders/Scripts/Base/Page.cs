using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Dev.UI
{
    public class Page : MonoBehaviour
    {
        public PageName nama;

        protected void OnEnable()
        {
            Button[] allButtons = GetComponentsInChildren<Button>();

            if(allButtons.Length > 0)
            {
                foreach (var item in allButtons)
                {
                    item.onClick.AddListener(() => AudioManager.Instance.MainkanSuara("klik"));
                }
            }
        }

        protected void ChangeScene(string namaScene)
        {
            SceneManager.LoadScene(namaScene);
        }
    }
}


public enum PageName
{
    Menu,
    Pengembang,
    Quit,
    Petunjuk,
    Pengaturan,
    Info,
    Kuis,
    Pendahuluan,
    ArLevel
}


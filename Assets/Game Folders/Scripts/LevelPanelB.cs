using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelPanelB : MonoBehaviour
{
    [SerializeField] private Button b_back;
    [SerializeField] private Button b_help;
    [SerializeField] private Button b_answer;

    [SerializeField] private TMP_Dropdown[] dropdownKanan;
    [SerializeField] private TMP_Dropdown[] dropdownKiri;

    [SerializeField] private GameObject panelResult;
    [SerializeField] private TMP_Text labelResult;

    [SerializeField] private GameObject panelInfo;
    [SerializeField] private Button b_closeInfo;

    [SerializeField] private Button b_arah1Kiri;
    [SerializeField] private Button b_arah2Kiri;
    [SerializeField] private Button b_arah3Kiri;

    [SerializeField] private Button b_arah1Kanan;
    [SerializeField] private Button b_arah2Kanan;
    [SerializeField] private Button b_arah3Kanan;

    private int kiri1 , kiri2 , kiri3 , kanan1 , kanan2 , kanan3;

    private void Start()
    {
        b_back.onClick.AddListener(() => gameObject.SetActive(false));
        b_help.onClick.AddListener(() => panelInfo.SetActive(true));
        b_closeInfo.onClick.AddListener(() => panelInfo.SetActive(false));
        b_answer.onClick.AddListener(CekJawaban);

        b_arah1Kiri.onClick.AddListener(() =>
        {
            kiri1++;
            if(kiri1 > 3)
            {
                kiri1 = 0;
            }
            b_arah1Kiri.transform.localEulerAngles = new Vector3(0f, 0f ,kiri1 * 90f);
        });
        b_arah2Kiri.onClick.AddListener(() =>
        {
            kiri2++;
            if (kiri2 > 3)
            {
                kiri2 = 0;
            }
            b_arah2Kiri.transform.localEulerAngles = new Vector3(0f, 0f , kiri2 * 90f);
        });
        b_arah3Kiri.onClick.AddListener(() =>
        {
            kiri3++;
            if (kiri3 > 3)
            {
                kiri3 = 0;
            }
            b_arah3Kiri.transform.localEulerAngles = new Vector3(0f, 0f, kiri3 * 90f);
        });
        b_arah1Kanan.onClick.AddListener(() =>
        {
            kanan1++;
            if (kanan1 > 3)
            {
                kanan1 = 0;
            }
            b_arah1Kanan.transform.localEulerAngles = new Vector3(0f, 0f, kanan1 * 90f);
        });
        b_arah2Kanan.onClick.AddListener(() =>
        {
            kanan2++;
            if (kanan2 > 3)
            {
                kanan2 = 0;
            }
            b_arah2Kanan.transform.localEulerAngles = new Vector3(0f, 0f, kanan2 * 90f);
        });
        b_arah3Kanan.onClick.AddListener(() =>
        {
            kanan3++;
            if (kanan3 > 3)
            {
                kanan3 = 0;
            }
            b_arah3Kanan.transform.localEulerAngles = new Vector3(0f, 0f, kanan3 * 90f);
        });
    }

    private void CekJawaban()
    {
        List<int> kanan = new List<int>();
        List<int> kiri = new List<int>();

        for (int i = 0; i < dropdownKanan.Length; i++)
        {
            kanan.Add(dropdownKanan[i].value);
        }
        for (int i = 0; i < dropdownKiri.Length; i++)
        {
            kiri.Add(dropdownKiri[i].value);
        }

        bool jawabanKanan = kanan[0] == 0 && kanan[1] == 1 && kanan[2] == 2 &&
            kanan[3] == 3;
        bool jawabanKiri = kiri[0] == 0 && kiri[1] == 1 && kiri[2] == 2 &&
            kiri[3] == 3;

        bool gambarKanan = kanan1 == 0 && kanan2 == 1 && kanan3 == 3;
        bool gambarKiri = kiri1 == 2 && kiri2 == 3 && kiri3 == 1;

        if (jawabanKiri && jawabanKanan && gambarKiri && gambarKanan)
        {
            MunculkanHasil(true);
        }
        else
        {
            MunculkanHasil(false);
        }
    }

    public void MunculkanHasil(bool benar)
    {
        panelResult.SetActive(true);
        if (benar)
        {
            AudioManager.Instance.MainkanSuara("win");
            labelResult.text = $"<color=green>Jawaban Benar !!";
        }
        else
        {
            AudioManager.Instance.MainkanSuara("lose");
            labelResult.text = $"<color=red>Jawaban Salah . .";
        }
        StartCoroutine(HideResult());
    }

    IEnumerator HideResult()
    {
        yield return new WaitForSeconds(5f);
        panelResult.SetActive(false);
    }
}

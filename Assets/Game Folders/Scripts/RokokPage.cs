using Dev.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RokokPage : Page
{
    [SerializeField] private LevelRokok selectedLevel;
    [SerializeField] private Epitel selectedEpitel;

    [SerializeField] private Button b_home;
    [SerializeField] private Button b_kanan;
    [SerializeField] private Button b_kiri;
    [SerializeField] private Button b_zoomIn;
    [SerializeField] private Button b_zoomOut;
    [SerializeField] private Button b_reset;
    [SerializeField] private Button b_closeKeterangan;
    [SerializeField] private Button b_playSound;
    [SerializeField] private Button b_opsi;
    [SerializeField] private Button b_closeOpsi;
    [SerializeField] private Button b_information;

    [SerializeField] private Button b_fxSistemPernapasan;
    [SerializeField] private Button b_fxEpitel;
    [SerializeField] private Button b_fxAlveolus;

    [SerializeField] private GameObject panelKeterangan;
    [SerializeField] private GameObject panelOpsi;

    [SerializeField] private GameObject[] models;

    [SerializeField] private GameObject SurfaceTracker;

    [SerializeField] private KeteranganRokok[] keterangans;
    [SerializeField] private KeteranganEpitel[] epitelDatas;

    [SerializeField] private TMP_Text labelTitle;
    [SerializeField] private TMP_Text labelDetail;

    private void Start()
    {
        AudioManager.Instance.HentikanSuara("BGM");
        SetupLevel(LevelRokok.EfekPernapasan);

        b_home.onClick.AddListener(() =>
        {
            ChangeScene("Main Menu");
            AudioManager.Instance.HentikanSuara("Efek Pernapasan");
            AudioManager.Instance.HentikanSuara("Efek Epitel");
            AudioManager.Instance.HentikanSuara("Efek Alveolus");

        });

        b_kanan.onClick.AddListener(() =>
        {
            PutarKanan(true);
        });

        b_kiri.onClick.AddListener(() =>
        {
            PutarKanan(false);
        });

        b_zoomIn.onClick.AddListener(() =>
        {
            ZoomIn(true);
        });

        b_zoomOut.onClick.AddListener(() =>
        {
            ZoomIn(false);
        });

        b_reset.onClick.AddListener(() =>
        {
            StartCoroutine(ResetTrack());
        });

        b_opsi.onClick.AddListener(() =>
        {
            panelOpsi.SetActive(true);
            b_fxSistemPernapasan.onClick.AddListener(() =>
            {
                SetupLevel(LevelRokok.EfekPernapasan);
                panelOpsi.SetActive(false);
            });
            b_fxEpitel.onClick.AddListener(() =>
            {
                SetupLevel(LevelRokok.EfekEpitel);
                panelOpsi.SetActive(false);
            });
            b_fxAlveolus.onClick.AddListener(() =>
            {
                SetupLevel(LevelRokok.EfekAlveolus);
                panelOpsi.SetActive(false);
            });
        });

        b_closeKeterangan.onClick.AddListener(() =>
        {
            panelKeterangan.SetActive(false);
            AudioManager.Instance.HentikanSuara("Efek Pernapasan");
            AudioManager.Instance.HentikanSuara("Efek Epitel");
            AudioManager.Instance.HentikanSuara("Efek Alveolus");
        });

        b_closeOpsi.onClick.AddListener(() => panelOpsi.SetActive(false));
    }

    void SetupLevel(LevelRokok level)
    {
        selectedLevel = level;

        foreach (var item in models)
        {
            item.SetActive(false);
        }

        switch (level)
        {
            case LevelRokok.EfekPernapasan:
                models[0].SetActive(true);
                b_information.gameObject.SetActive(true);
                b_information.onClick.AddListener(() => SetKeterangan(LevelRokok.EfekPernapasan));

                b_playSound.onClick.AddListener(() =>
                {
                    AudioManager.Instance.MainkanSuara("Efek Pernapasan");
                    AudioManager.Instance.HentikanSuara("Efek Epitel");
                    AudioManager.Instance.HentikanSuara("Efek Alveolus");
                });
                break;
            case LevelRokok.EfekEpitel:
                models[1].SetActive(true);
                b_information.gameObject.SetActive(true);
                b_information.onClick.AddListener(() => SetKeterangan(LevelRokok.EfekEpitel));

                b_playSound.onClick.AddListener(() =>
                {
                    AudioManager.Instance.HentikanSuara("Efek Pernapasan");
                    AudioManager.Instance.MainkanSuara("Efek Epitel");
                    AudioManager.Instance.HentikanSuara("Efek Alveolus");
                });
                break;
            case LevelRokok.EfekAlveolus:
                models[2].SetActive(true);
                b_information.gameObject.SetActive(true);
                b_information.onClick.AddListener(() => SetKeterangan(LevelRokok.EfekAlveolus));
                b_playSound.onClick.AddListener(() =>
                {
                    AudioManager.Instance.HentikanSuara("Efek Pernapasan");
                    AudioManager.Instance.HentikanSuara("Efek Epitel");
                    AudioManager.Instance.MainkanSuara("Efek Alveolus");
                });
                break;
        }
    }

    public void SetKeterangan(LevelRokok level)
    {
        panelKeterangan.SetActive(true);
        string detail = string.Empty;

        KeteranganRokok d = Array.Find(keterangans, p => p.level == level);
        detail = d.detail;

        labelTitle.text = d.nama;
        labelDetail.text = detail;
    }

    public void SetDetailEpitel(Epitel nama)
    {
        selectedEpitel = nama;
        panelKeterangan.SetActive(true);
        string detail = string.Empty;

        KeteranganEpitel d = Array.Find(epitelDatas, p => p.level == nama);
        detail = d.detail;

        labelTitle.text = d.nama;
        labelDetail.text = detail;
    }

    IEnumerator ResetTrack()
    {
        SurfaceTracker.SetActive(false);
        yield return new WaitForSeconds(1f);
        SurfaceTracker.SetActive(true);
    }

    private void PutarKanan(bool iskanan)
    {
        if (iskanan)
        {
            foreach (var model in models)
            {
                model.transform.Rotate(0f, 2f, 0f);
            }
        }
        else
        {
            foreach (var model in models)
            {
                model.transform.Rotate(0f, -2f, 0f);
            }
        }
    }

    void ZoomIn(bool isZoomIn)
    {
        if (isZoomIn)
        {
            foreach (var model in models)
            {
                model.transform.localScale *= 1.1f;
            }
        }
        else
        {
            foreach (var model in models)
            {
                model.transform.localScale *= 0.9f;
            }
        }
    }
}



public enum LevelRokok
{
    EfekPernapasan,
    EfekEpitel,
    EfekAlveolus
}

[System.Serializable]
public class KeteranganRokok
{
    public LevelRokok level;
    public string nama;
    [TextArea(5 , 15)] public string detail;
}

[System.Serializable]
public class KeteranganEpitel
{
    public Epitel level;
    public string nama;
    [TextArea(5, 15)] public string detail;
}

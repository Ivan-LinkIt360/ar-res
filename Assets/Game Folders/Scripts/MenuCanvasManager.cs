using Dev.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCanvasManager : CanvasManager
{
    private void OnEnable()
    {
        GameManager.Instance.OnStateChange += Instance_OnStateChange;
    }
    private void OnDisable()
    {
        GameManager.Instance.OnStateChange -= Instance_OnStateChange;
    }

    private void Instance_OnStateChange(GameState newState)
    {
        switch (newState)
        {
            case GameState.Menu:
                SetPage(PageName.Menu);
                break;
            case GameState.Pengembang:
                SetPage(PageName.Pengembang);
                break;
            case GameState.Quit:
                SetPage(PageName.Quit);
                break;
            case GameState.Pendahuluan:
                SetPage(PageName.Pendahuluan);
                break;
            case GameState.Petunjuk:
                SetPage(PageName.Petunjuk);
                break;
            case GameState.KameraAr:
                SetPage(PageName.ArLevel);
                break;
            case GameState.Kuis:
                SetPage(PageName.Kuis);
                break;
            case GameState.Info:
                SetPage(PageName.Info);
                break;
            case GameState.Pengaturan:
                SetPage(PageName.Pengaturan);
                break;
        }
    }
}

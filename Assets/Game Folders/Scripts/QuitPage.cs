using Dev.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitPage : Page
{
    [SerializeField] private Button b_ya;
    [SerializeField] private Button b_tidak;

    private void Start()
    {
        b_ya.onClick.AddListener(() => Application.Quit());
        b_tidak.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));
    }
}

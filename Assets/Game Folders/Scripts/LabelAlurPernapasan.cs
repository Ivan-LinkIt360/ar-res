using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LabelAlurPernapasan : MonoBehaviour
{
    private Transform cam;
    [SerializeField] private Organ nama;

    [SerializeField] private GamePage page;

    private void Start()
    {
        cam = Camera.main.transform;
    }

    private void Update()
    {
        transform.LookAt(2 * transform.position - cam.position);
    }

    private void OnMouseEnter()
    {
        page.SetDetailOrgan(nama);
        GetComponent<TMP_Text>().color = Color.red;
    }

    private void OnMouseExit()
    {
        GetComponent<TMP_Text>().color = Color.white;
    }
}

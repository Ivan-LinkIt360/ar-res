﻿public enum Organ
{
    RonggaHidung,
    RonggaMulut,
    Hidung,
    Epiglotis,
    Diafragma,
    Faring,
    Laring,
    Trakea,
    Bronkus,
    Bronkiolus,
    Alveolus,
    ParuParu
}

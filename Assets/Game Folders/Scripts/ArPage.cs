using Dev.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArPage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private Button b_levelA;
    [SerializeField] private Button b_levelB;

    private void Start()
    {
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        b_levelA.onClick.AddListener(() => ChangeScene("AR Scene"));
        b_levelB.onClick.AddListener(() => ChangeScene("AR Scene Rokok"));
    }
}

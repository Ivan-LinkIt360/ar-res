﻿using UnityEngine;

public partial class GamePage
{
    [System.Serializable]
    public class OrganDetail
    {
        public Organ organ;
        public string namaOrgan;
        [TextArea(5,10)] public string detail;
    }
}

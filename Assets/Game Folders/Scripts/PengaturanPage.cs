using Dev.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PengaturanPage : Page
{
    [SerializeField] private Button b_home;
    [SerializeField] private Slider s_music;
    [SerializeField] private Slider s_sfx;

    private void Start()
    {
        b_home.onClick.AddListener(() => GameManager.Instance.ChangeState(GameState.Menu));

        s_music.value = AudioManager.Instance.GetAudioVolume();
        s_sfx.value = AudioManager.Instance.GetSfxVolume();

        s_music.onValueChanged.AddListener(HandleAudioVolume);
        s_sfx.onValueChanged.AddListener(HandleSfxVolume);
    }

    private void HandleSfxVolume(float arg0)
    {
        PlayerPrefs.SetFloat("Sfx", arg0);
        AudioManager.Instance.SetVolume();
    }

    private void HandleAudioVolume(float arg0)
    {
        PlayerPrefs.SetFloat("Audio", arg0);
        AudioManager.Instance.SetVolume();
    }
}

using Dev.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PembukaanPage : Page
{
    [SerializeField] private Button b_mulai;
    [SerializeField] private string namaScene;

    private void Start()
    {
        b_mulai.onClick.AddListener(() =>
        {
            ChangeScene(namaScene);
        });

        AudioManager.Instance.MainkanSuara("BGM");
    }
}

using Dev.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public partial class GamePage : Page
{
    [SerializeField] private Organ selectedOrgan;
    [SerializeField] private LevelPernapasan selectedLevel;

    [SerializeField] private Button b_home;
    [SerializeField] private Button b_kanan;
    [SerializeField] private Button b_kiri;
    [SerializeField] private Button b_zoomIn;
    [SerializeField] private Button b_zoomOut;
    [SerializeField] private Button b_reset;
    [SerializeField] private Button b_closeKeterangan;
    [SerializeField] private Button b_playSound;
    [SerializeField] private Button b_opsi;
    [SerializeField] private Button b_closeOpsi;
    [SerializeField] private Button b_closeDetail;
    [SerializeField] private Button b_information;

    [SerializeField] private Button b_strukturFungsi;
    [SerializeField] private Button b_alurUdara;
    [SerializeField] private Button b_mekanismePernapasan;

    [SerializeField] private Button b_inspirasi;
    [SerializeField] private Button b_ekspirasi;

    [SerializeField] private GameObject[] models;
    [SerializeField] private GameObject[] modelPernapasan;

    [SerializeField] private GameObject panelKeteranganOrgan;
    [SerializeField] private GameObject panelOpsi;
    [SerializeField] private GameObject panelDetail;
    [SerializeField] private GameObject panelPernapasan;

    [SerializeField] private TMP_Text label_titleOrgan;
    [SerializeField] private TMP_Text label_keteranganOrgan;
    [SerializeField] private TMP_Text label_detail;

    [SerializeField] private OrganDetail[] details;
    [SerializeField] private TipePernapasan[] pernapasan;

    [SerializeField] private GameObject SurfaceTracker;

    private void Start()
    {
        AudioManager.Instance.HentikanSuara("BGM");
        SetupLevel(LevelPernapasan.strukturFungsi);

        b_home.onClick.AddListener(() =>
        {
            AudioManager.Instance.HentikanSuara(selectedOrgan.ToString());
            AudioManager.Instance.HentikanSuara("Alur Pernapasan");
            AudioManager.Instance.HentikanSuara(pernapasan[0].nama);
            AudioManager.Instance.HentikanSuara(pernapasan[1].nama);

            ChangeScene("Main Menu");
        });

        b_kanan.onClick.AddListener(() =>
        {
            PutarKanan(true);
        });

        b_kiri.onClick.AddListener(() =>
        {
            PutarKanan(false);
        });

        b_zoomIn.onClick.AddListener(() =>
        {
            ZoomIn(true);
        });

        b_zoomOut.onClick.AddListener(() =>
        {
            ZoomIn(false);
        });

        b_reset.onClick.AddListener(() =>
        {
            StartCoroutine(ResetTrack());
        });

        b_opsi.onClick.AddListener(() =>
        {
            panelOpsi.SetActive(true);
            b_strukturFungsi.onClick.AddListener(() =>
            {
                SetupLevel(LevelPernapasan.strukturFungsi);
                panelOpsi.SetActive(false);
            });
            b_alurUdara.onClick.AddListener(() =>
            {
                SetupLevel(LevelPernapasan.alurPernapasan);
                panelOpsi.SetActive(false);
            });
            b_mekanismePernapasan.onClick.AddListener(() =>
            {
                SetupLevel(LevelPernapasan.mekanismePernapasan);
                panelOpsi.SetActive(false);
            });
        });

        b_closeOpsi.onClick.AddListener(() => panelOpsi.SetActive(false));
        b_closeDetail.onClick.AddListener(() => panelDetail.SetActive(false));

        b_closeKeterangan.onClick.AddListener(() =>
        {
            panelKeteranganOrgan.SetActive(false);
            AudioManager.Instance.HentikanSuara(selectedOrgan.ToString());
            AudioManager.Instance.HentikanSuara("Alur Pernapasan");
            AudioManager.Instance.HentikanSuara(pernapasan[0].nama);
            AudioManager.Instance.HentikanSuara(pernapasan[1].nama);
        });  
    }

    private void SetInformation(LevelPernapasan selectedLevel)
    {
        panelKeteranganOrgan.SetActive(true);

        switch (selectedLevel)
        {
            case LevelPernapasan.strukturFungsi:
                break;
            case LevelPernapasan.alurPernapasan:
                label_titleOrgan.text = "Alur Udara Pernapasan";
                label_keteranganOrgan.text = $"Selain berperan dalam pertukaran gas O2 dan CO2 sistem pernapasan juga berperan untuk:<br>" +
                    $"1. Merupakan jalur pengeluaran air dan panas<br>" +
                    $"2. Membantu mempertahankan keseimbangan asam-basa dengan mengubah jumlah CO2 dan H2CO2 sebagai penghasil ion H-<br>" +
                    $"3. Memungkinkan berbicara, menyanyi, maupun pembentukan vokal lainnya<br>" +
                    $"4. Merupakan sistem pertahanan terhadap benda asing yang terhirup<br>" +
                    $"5. Mengeluarkan, memodifikasi, mengaktifkan atau menginaktifkan berbagai bahan yang mengalir melewati sirkulasi paru-paru<br>" +
                    $"6. sebagai indera penciuman, yang dilakukan organ pernapasan hidung";
                break;
            case LevelPernapasan.mekanismePernapasan:
                break;
        }
    }

    IEnumerator ResetTrack()
    {
        SurfaceTracker.SetActive(false);
        yield return new WaitForSeconds(1f);
        SurfaceTracker.SetActive(true);
    }
    private void SetupLevel(LevelPernapasan level)
    {
        selectedLevel = level;

        foreach (var item in models)
        {
            item.SetActive(false);
        }
        switch (level)
        {
            case LevelPernapasan.strukturFungsi:
                models[0].SetActive(true);
                panelPernapasan.SetActive(false);

                b_information.gameObject.SetActive(false);

                b_playSound.onClick.AddListener(() =>
                {
                    AudioManager.Instance.MainkanSuara(selectedOrgan.ToString());
                    AudioManager.Instance.HentikanSuara("Alur Pernapasan");
                    AudioManager.Instance.HentikanSuara(pernapasan[0].nama);
                    AudioManager.Instance.HentikanSuara(pernapasan[1].nama);
                });
                break;
            case LevelPernapasan.alurPernapasan:
                models[1].SetActive(true);
                panelPernapasan.SetActive(false);

                b_information.gameObject.SetActive(true);
                b_information.onClick.AddListener(() => SetInformation(selectedLevel));

                b_playSound.onClick.AddListener(() =>
                {
                    AudioManager.Instance.HentikanSuara(selectedOrgan.ToString());
                    AudioManager.Instance.MainkanSuara("Alur Pernapasan");
                    AudioManager.Instance.HentikanSuara(pernapasan[0].nama);
                    AudioManager.Instance.HentikanSuara(pernapasan[1].nama);
                });
                break;
            case LevelPernapasan.mekanismePernapasan:
                models[2].SetActive(true);
                panelPernapasan.SetActive(true);
                b_information.gameObject.SetActive(false);

                b_inspirasi.onClick.AddListener(() =>
                {
                    b_information.gameObject.SetActive(true);
                    b_information.onClick.AddListener(() =>
                    {
                        panelKeteranganOrgan.SetActive(true);
                        label_titleOrgan.text = pernapasan[0].nama;
                        label_keteranganOrgan.text = pernapasan[0].detail;

                    });

                    b_playSound.onClick.AddListener(() =>
                    {
                        AudioManager.Instance.HentikanSuara(selectedOrgan.ToString());
                        AudioManager.Instance.HentikanSuara("Alur Pernapasan");
                        AudioManager.Instance.MainkanSuara(pernapasan[0].nama);
                        AudioManager.Instance.HentikanSuara(pernapasan[1].nama);
                    });
                    foreach (var item in modelPernapasan)
                    {
                        item.SetActive(false);
                    }
                    modelPernapasan[1].SetActive(true);
                });
                b_ekspirasi.onClick.AddListener(() =>
                {
                    b_information.gameObject.SetActive(true);
                    b_information.onClick.AddListener(() =>
                    {
                        panelKeteranganOrgan.SetActive(true);
                        label_titleOrgan.text = pernapasan[1].nama;
                        label_keteranganOrgan.text = pernapasan[1].detail;

                    });

                    b_playSound.onClick.AddListener(() =>
                    {
                        AudioManager.Instance.HentikanSuara(selectedOrgan.ToString());
                        AudioManager.Instance.HentikanSuara("Alur Pernapasan");
                        AudioManager.Instance.HentikanSuara(pernapasan[0].nama);
                        AudioManager.Instance.MainkanSuara(pernapasan[1].nama);
                    });

                    foreach (var item in modelPernapasan)
                    {
                        item.SetActive(false);
                    }
                    modelPernapasan[2].SetActive(true);
                });

                break;
        }
    }

    private void PutarKanan(bool iskanan)
    {
        if (iskanan)
        {
            foreach (var model in models)
            {
                model.transform.Rotate(0f, 2f, 0f);
            }
        }
        else
        {
            foreach (var model in models)
            {
                model.transform.Rotate(0f, -2f, 0f);
            }
        }
    }

    void ZoomIn(bool isZoomIn)
    {
        if (isZoomIn)
        {
            foreach (var model in models)
            {
                model.transform.localScale *= 1.1f;
            }
        }
        else
        {
            foreach (var model in models)
            {
                model.transform.localScale *= 0.9f;
            }
        }
    }

    public void SetKeteranganOrgan(Organ organName)
    {
        selectedOrgan = organName;
        panelKeteranganOrgan.SetActive(true);
        string detail = string.Empty;

        OrganDetail d = Array.Find(details, p => p.organ == organName);
        detail = d.detail;

        label_titleOrgan.text = d.namaOrgan;
        label_keteranganOrgan.text = detail;
    }

    public void SetDetailOrgan(Organ organName)
    {
        panelDetail.SetActive(true);
        string d = string.Empty;

        switch (organName)
        {
            case Organ.RonggaMulut:
                d = "Melembabkan, menghangatkan, dan membersihkan udara yang dihirup";
                break;
            case Organ.Faring:
                d = "Saluran bersama sistem pernapasan dan sistem pencernaan";
                break;
            case Organ.Laring:
                d = "Menghasilkan suara dari udara ekspirasi";
                break;
            case Organ.Trakea:
                d = "Saluran utama udara menuju dan keluar dari paru-paru yang dilapisi oleh epitel bersilia, berfungsi menyapu partikel asing";
                break;
            case Organ.Bronkus:
                d = "Cabang trakea sebagai saluran udara lanjutan dari trakea";
                break;
            case Organ.Bronkiolus:
                d = "Cabang bronkus sebagai saluran udara menuju alveolus";
                break;
            case Organ.Alveolus:
                d = "Tempat pertukaran gas oksigen dan karbondioksida";
                break;
        }

        label_detail.text = d;
    }
}

[System.Serializable]
public class TipePernapasan
{
    public string nama;

    [TextArea(10 , 20)] public string detail;
}
